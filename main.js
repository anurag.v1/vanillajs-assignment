const input=document.querySelector("textarea")
const btn=document.querySelector(".save")
console.log(input);
var count=0;
var count2=0;
btn.addEventListener('click',addInList);

function addInList(e){
    const completed=document.querySelector(".done");
    const notCompleted=document.querySelector(".pending");
    const list=document.createElement('li');
    const doneBtn=document.createElement('button');
    const deleteBtn=document.createElement('button');
    const docount=document.querySelector('.docounter');
    const tskcount=document.getElementById('counter');

    
    deleteBtn.innerHTML='<i class="fa fa-times" aria-hidden="true"></i>';
    doneBtn.innerHTML='<button class="mark">Mark as Done</button>';

    if(input.value!==''){
        list.textContent=input.value;
        input.value='';
        notCompleted.appendChild(list);
        count=count+1;
        tskcount.innerHTML=count;        
        list.appendChild(deleteBtn);
        list.appendChild(doneBtn);
    }

    doneBtn.addEventListener('click', function(){
        const parent=this.parentNode;
        parent.classList.add("taskdone");
        parent.remove();
        count=count-1;
        count2=count2+1;
        tskcount.innerHTML=count; 
        docount.innerHTML=count2; 
        completed.appendChild(parent);
        doneBtn.style.display='none';
    });

    deleteBtn.addEventListener('click',function(){
        const parent=this.parentNode;
        if(parent.matches('.taskdone')){
            count2=count2-1;
            docount.innerHTML=count2; 
        }
        else{
            count=count-1;
            tskcount.innerHTML=count;
        }
        console.log(parent);
        parent.remove();

    });

}
